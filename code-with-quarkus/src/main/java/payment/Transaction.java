package payment;

public class Transaction {
    public String cid, mid;
    public int amount;

    public Transaction() {}

    public Transaction(String cid, String mid, int amount) {
        this.cid = cid;
        this.mid = mid;
        this.amount = amount;
    }
}