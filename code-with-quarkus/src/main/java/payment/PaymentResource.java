package payment;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Path("/payments")
public class PaymentResource {
    List<Transaction> transactions = new ArrayList<>();
    List<String> customers = Arrays.asList("cid1");
    List<String> merchants = Arrays.asList("mid1");

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get() {
        return Response.status(200).entity(transactions).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response pay(Transaction transaction) {
        if (!customers.contains(transaction.cid)) {
            return Response.status(404).entity("customer with id " + transaction.cid + " is unknown").build();
        }

        if (!merchants.contains(transaction.mid)) {
            return Response.status(404).entity("merchant with id " + transaction.mid + " is unknown").build();
        }

        if (!transactions.stream().anyMatch(t -> t.cid.equals(transaction.cid) && t.mid.equals(transaction.mid) && t.amount == transaction.amount)) {
            transactions.add(transaction);
        }

        return Response.status(201).build();
    }
}