package bank.dto;

import dtu.ws.fastmoney.User;

import java.math.BigDecimal;

public class CreateAccountDto extends User {
    public BigDecimal balance;

    public CreateAccountDto() {}

    public BigDecimal getBalance() {
        return this.balance;
    }
}
