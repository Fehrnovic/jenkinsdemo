package bank.dto;

import java.math.BigDecimal;

public class CreatePaymentDto {
    public String customerCpr, merchantCpr, description;
    public BigDecimal amount;

    public CreatePaymentDto() {}
}
