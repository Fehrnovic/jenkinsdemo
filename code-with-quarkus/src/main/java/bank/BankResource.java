package bank;

import bank.dto.CreateAccountDto;
import bank.dto.CreatePaymentDto;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/bank")
public class BankResource {
    BankService _bankService;

    public BankResource() {
        var bankServiceFetcher = new BankServiceService();
        this._bankService = bankServiceFetcher.getBankServicePort();
    }

    @Path("/test")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response test() {
        return Response.status(200).entity("Hej med dig").build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccounts() {
        var accounts = _bankService.getAccounts();

        return Response.status(200).entity(accounts).build();
    }

    @Path("/balance/{cprNumber}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBalance(@PathParam("cprNumber") String cprNumber) {
        try {
            var account = _bankService.getAccountByCprNumber(cprNumber);

            return Response.status(200).entity(account.getBalance()).build();
        } catch (BankServiceException_Exception e) {
            return Response.status(404).entity("account not found").build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createAccount(CreateAccountDto createAccountDto) {
        //delete user if exists
        try {
            var existingUser = _bankService.getAccountByCprNumber(createAccountDto.getCprNumber());
            if (existingUser != null) {
                _bankService.retireAccount(existingUser.getId());
            }
        } catch (BankServiceException_Exception e) {}

        User user = new User();
        user.setCprNumber(createAccountDto.getCprNumber());
        user.setFirstName(createAccountDto.getFirstName());
        user.setLastName(createAccountDto.getLastName());

        try {
            var response = _bankService.createAccountWithBalance(user, createAccountDto.getBalance());
        } catch (BankServiceException_Exception e) {
            return Response.status(400).entity(e.getMessage()).build();
        }

        return Response.status(201).build();
    }

    @Path("/payment")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createPayment(CreatePaymentDto createPaymentDto) {
        try {
            var customer = _bankService.getAccountByCprNumber(createPaymentDto.customerCpr);
            var merchant = _bankService.getAccountByCprNumber(createPaymentDto.merchantCpr);

            _bankService.transferMoneyFromTo(customer.getId(), merchant.getId(), createPaymentDto.amount, createPaymentDto.description);
        } catch (BankServiceException_Exception e) {
            return Response.status(400).entity(e.getMessage()).build();
        }

        return Response.status(201).build();
    }
}