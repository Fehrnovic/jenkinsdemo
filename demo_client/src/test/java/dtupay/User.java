package dtupay;

public class User {
    public String cprNumber;
    public String firstName;
    public String lastName;

    public User(String firstName, String lastName, String cprNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.cprNumber = cprNumber;
    }
}
