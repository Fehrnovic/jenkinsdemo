package dtupay;

import javax.json.Json;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.GenericType;
import java.util.List;

public class BankPaymentService {

    WebTarget baseUrl;

    public BankPaymentService() {
        Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:8080/");
    }

    public boolean createAccount(User user, int balance) {
        var jsonObject = Json.createObjectBuilder()
                .add("firstName", user.firstName)
                .add("lastName", user.lastName)
                .add("cprNumber", user.cprNumber)
                .add("balance", balance)
                .build();

        var response = baseUrl.path("bank").request().post(Entity.json(jsonObject));

        return response.getStatus() == 201;
    }

    public boolean createPayment(String customerCpr, String merchantCpr, int amount, String description) {
        var jsonObject = Json.createObjectBuilder()
                .add("customerCpr", customerCpr)
                .add("merchantCpr", merchantCpr)
                .add("amount", amount)
                .add("description", description)
                .build();

        var response = baseUrl.path("bank/payment").request().post(Entity.json(jsonObject));

        return response.getStatus() == 201;
    }

    public int getBalance(String cprNumber) {
        var response = baseUrl.path("bank/balance/" + cprNumber).request().get();

        return response.readEntity(Integer.class);
    }
}
