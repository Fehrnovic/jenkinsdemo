package dtupay;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.GenericType;
import java.util.List;

public class PaymentService {

    WebTarget baseUrl;

    public PaymentService() {
        Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:8080/");
    }

    public boolean pay(String cid, String mid, int amount) {
        var transaction = new Transaction(cid, mid, amount);
        var json = Entity.json(transaction);

        var response = baseUrl.path("payments").request().post(json);

        if (response.getStatus() == 404) {
            var errorMessage = response.readEntity(String.class);

            throw new NotFoundException(errorMessage);
        }

        return response.getStatus() == 201;
    }

    public List<Transaction> getTransactions() {
        return baseUrl.path("payments").request().get(new GenericType<>() {});
    }
}
