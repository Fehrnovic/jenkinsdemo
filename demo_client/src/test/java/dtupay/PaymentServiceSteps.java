package dtupay;

import static org.junit.jupiter.api.Assertions.*;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import javax.ws.rs.NotFoundException;
import java.util.List;

/* Hint:
 * The step classes do not do the HTTP requests themselves.
 * Instead, the tests use the class HelloService, which encapsulates the
 * HTTP requests. This abstractions help to write easier and more understandable
 * test classes.
 */

public class PaymentServiceSteps {
    String cid, mid, errorMessage;
    PaymentService dtuPay = new PaymentService();
    boolean successful;
    List<Transaction> transactions;

    @Given("a customer with id {string}")
    public void aCustomerWithId(String cid) {
        this.cid = cid;
    }

    @Given("a merchant with id {string}")
    public void aMerchantWithId(String mid) {
        this.mid = mid;
    }

    @When("the merchant initiates a payment for {int} kr by the customer")
    public void theMerchantInitiatesAPaymentForKrByTheCustomer(int amount) {
        try {
            successful = dtuPay.pay(cid, mid, amount);
        } catch (NotFoundException e) {
            errorMessage = e.getMessage();
        }
    }

    @Then("the payment is successful.")
    public void thePaymentIsSuccessful() {
        assertTrue(successful);
    }

    @Given("a successful payment of {int} kr from customer {string} to merchant {string}")
    public void aSuccessfulPaymentOfKrFromCustomerToMerchant(int amount, String cid, String mid) {
        assertTrue(dtuPay.pay(cid, mid, amount));
    }

    @When("the manager asks for a list of transactions")
    public void theManagerAsksForAListOfTransactions() {
        transactions = dtuPay.getTransactions();
    }

    @Then("the list contains a transaction where customer {string} paid {int} kr to merchant {string}.")
    public void theListContainsATransactionWhereCustomerPaidKrToMerchant(String cid, int amount, String mid) {
        assertTrue(transactions.stream().anyMatch(t -> t.cid.equals(cid) && t.mid.equals(mid) && t.amount == amount));
    }

    @Then("the payment is not successful")
    public void thePaymentIsNotSuccessful() {
        assertFalse(successful);
    }

    @And("an error message is returned saying {string}.")
    public void anErrorMessageIsReturnedSaying(String expectedErrorMessage) {
        assertEquals(expectedErrorMessage, errorMessage);
    }
}
