package dtupay;

import static org.junit.jupiter.api.Assertions.*;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import javax.ws.rs.NotFoundException;
import java.util.List;

/* Hint:
 * The step classes do not do the HTTP requests themselves.
 * Instead, the tests use the class HelloService, which encapsulates the
 * HTTP requests. This abstractions help to write easier and more understandable
 * test classes.
 */

public class BankPaymentServiceSteps {
    BankPaymentService bankPaymentService = new BankPaymentService();

    public User customer, merchant;
    public int balance;
    public boolean successful;

    @Given("the customer {string} {string} with CPR {string} has a bank account")
    public void theCustomerHasABankAccount(String firstname, String lastname, String cpr) {
        customer = new User(firstname, lastname, cpr);
    }

    @And("the balance of that account is {int}")
    public void theBalanceOfThatAccountIs(int balance) {
        this.balance = balance;
    }

    @And("the customer is registered with DTUPay")
    public void theCustomerIsRegisteredWithDTUPay() {
        assertTrue(bankPaymentService.createAccount(customer, balance));
    }

    @And("the merchant {string} {string} with CPR number {string} has a bank account")
    public void theMerchantWithCPRNumberHasABankAccount(String firstname, String lastname, String cpr) {
        merchant = new User(firstname, lastname, cpr);
    }

    @And("the merchant is registered with DTUPay")
    public void theMerchantIsRegisteredWithDTUPay() {
        assertTrue(bankPaymentService.createAccount(merchant, balance));
    }

    @When("the merchant initiates a payment at the bank for {int} kr by the customer")
    public void theMerchantInitiatesAPaymentAtTheBankForKrByTheCustomer(int amount) {
        successful = bankPaymentService.createPayment(customer.cprNumber, merchant.cprNumber, amount, "trade of the year");
    }

    @Then("the payment is successful")
    public void thePaymentIsSuccessful() {
        assertTrue(successful);
    }

    @And("the balance of the customer at the bank is {int} kr")
    public void theBalanceOfTheCustomerAtTheBankIsKr(int balance) {
        assertEquals(bankPaymentService.getBalance(customer.cprNumber), balance);
    }

    @And("the balance of the merchant at the bank is {int}")
    public void theBalanceOfTheMerchantAtTheBankIs(int balance) {
        assertEquals(bankPaymentService.getBalance(merchant.cprNumber), balance);
    }
}
